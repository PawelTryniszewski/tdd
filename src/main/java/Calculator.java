public class Calculator {


    public Integer add(Integer a,Integer b){
        verify(a,b);
        return a+b;
    }public Integer subtract (Integer a, Integer b){
        verify(a,b);
        return a-b;
    }public Integer multiple (Integer a, Integer b){
        verify(a,b);
        return a*b;
    }public Integer divide(Integer a, Integer b){
        verify(a,b);
        if (b==0){
            throw  new IllegalArgumentException("Divisor cannot be 0.");
        }

        return a/b;
    }public boolean isOdd(Integer a){
        if (a%2==0){
            return true;
        }
        return false;
    }
    public void verify(Integer a, Integer b){
        if (a==null||b==null){
            throw new IllegalArgumentException("cannot be null.");
        }
    }

}
