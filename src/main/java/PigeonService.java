public interface PigeonService {
    public boolean isAvaliable();
    public void sendMessage(String message);
}
