import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * allSwap(["ab", "ac"]) → ["ac", "ab"]
 * allSwap(["ax", "bx", "cx", "cy", "by", "ay", "aaa", "azz"]) → ["ay", "by", "cy", "cx", "bx", "ax", "azz", "aaa"]
 * allSwap(["ax", "bx", "ay", "by", "ai", "aj", "bx", "by"]) → ["ay", "by", "ax", "bx", "aj", "ai", "by", "bx"]
 */

public class zadanie2 {
    public static String[] allSwap(String[] strings) {
        Map<String, Integer> map = new HashMap();
        String firstChar = "";
        for (int i = 0; i < strings.length; i++) {
            firstChar = strings[i].substring(0, 1);
            if (!map.containsKey(firstChar)) {
                map.put(firstChar, i);
            } else if (map.containsKey(firstChar) && map.get(firstChar) != strings.length) {
                String temp = strings[map.get(firstChar)];
                strings[map.get(firstChar)] = strings[i];
                strings[i] = temp;
                map.put(firstChar, strings.length);
            } else {
                map.put(firstChar, i);
            }
        }
        return strings;
    }

    public static void main(String[] args) {

        String[] cos1 = new String[]{"ab", "ac"};
        String[] cos2 = new String[]{"ax", "bx", "cx", "cy", "by", "ay", "aaa", "azz"};
        String[] cos3 = new String[]{"ax", "bx", "ay", "by", "ai", "aj", "bx", "by"};
        System.out.println(Arrays.toString(allSwap(cos1)));
        System.out.println(Arrays.toString(allSwap(cos2)));
        System.out.println(Arrays.toString(allSwap(cos3)));
    }
}
