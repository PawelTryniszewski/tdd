import java.util.Arrays;

public class Mediana {


    public int mediana(int[] tab) {
        Arrays.sort(tab);
        if (tab.length==0){
            return -1;
        }
        if (tab.length==1){
            return tab[0];
        }
        if (tab.length % 2 == 0) {

//            int mediana = (((tab[(tab.length / 2) - 1]) + (tab[tab.length / 2])) / 2);
            int mediana = (tab[tab.length/2]-1);
            return mediana;

        }
        else {
           //int mediana = tab[(tab.length - 1) / 2];
            int mediana = tab[tab.length/2];
           return mediana;
        }


    }

}
