public class NotyficationService {
    private EmailService emailService;
    private PigeonService pigeonService;

    public NotyficationService(EmailService emailService, PigeonService pigeonService) {
        this.emailService = emailService;
        this.pigeonService = pigeonService;
    }

    public void sendNotification(String message) {
        String warning = "Nie mozna wyslac";
        if (emailService.isAvalible()) {
            emailService.sendEmail(message);
        } else if (pigeonService.isAvaliable()) {
            pigeonService.sendMessage(message);
        } else {
            if (!pigeonService.isAvaliable()) {
                pigeonService.sendMessage(warning);
            }
            if (!emailService.isAvalible()) {
                emailService.sendEmail(warning);
            }
        }
    }
}
