package Gra;

import net.bytebuddy.implementation.bytecode.Throw;

public class Plansza {
    private String[][] plansza;
    private int rozmiar = 3;

    public String[][] getPlansza() {
        return plansza;
    }

    public Plansza() {
        this.plansza = new String[rozmiar][rozmiar];

    }


    public boolean isEmpty() {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (plansza[i][j] != null) {
                    return false;
                }
            }

        }
        return true;
    }

    public void addElement(String string, int x, int y) {

        if (string.equals("x") || string.equals("o")) {
            if (x >= 0 && x <= 3 && y >= 0 && y <= 3) {
                if (plansza[x][y] == null) {
                    plansza[x][y] = string;
                }
            } else throw new ArrayIndexOutOfBoundsException("Nie ma takiego miejsca.");

        }
    }

    public String getIndex(int x, int y) {
        if (x >= 0 && x <= 3 && y >= 0 && y <= 3) {

            return plansza[x][y];
        }else throw new ArrayIndexOutOfBoundsException("Nie ma takiego miejsca.");
    }
}
