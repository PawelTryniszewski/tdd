package Gra;

public class PlanszaChecker {


    public static boolean isWinner(Plansza plansza1) {
        if (!plansza1.isEmpty()) {
            return true;
        }
        return false;
    }

    public static String winner(Plansza plansza) {

        if (plansza.isEmpty()) {
            return null;
        } else {
            for (int i = 0; i < 3; i++) {
                if ((plansza.getPlansza()[i][0] != null && plansza.getPlansza()[i][1] != null && plansza.getPlansza()[i][2] != null) ||
                        (plansza.getPlansza()[0][i] != null && plansza.getPlansza()[1][i] != null && plansza.getPlansza()[2][i] != null)) {

                    if (plansza.getPlansza()[i][0].equals(plansza.getPlansza()[i][1]) &&
                            plansza.getPlansza()[i][1].equals(plansza.getPlansza()[i][2])) {
                        return plansza.getPlansza()[i][0];
                    }
                    if (plansza.getPlansza()[0][i].equals(plansza.getPlansza()[1][i]) &&
                            plansza.getPlansza()[1][i].equals(plansza.getPlansza()[2][i])) {
                        return plansza.getPlansza()[0][i];
                    }
                }
                return null;
            }
        }
        return null;
    }

    public static String winnerCross(Plansza plansza) {
        if (plansza.getPlansza()[0][0] != null && plansza.getPlansza()[1][1] != null && plansza.getPlansza()[2][2] != null) {

            if (plansza.getPlansza()[0][0].equals(plansza.getPlansza()[1][1]) && plansza.getPlansza()[1][1]
                    .equals(plansza.getPlansza()[2][2])) {
                return plansza.getPlansza()[0][0];
            }
        }
        if (plansza.getPlansza()[2][0] != null && plansza.getPlansza()[1][1] != null && plansza.getPlansza()[0][2] != null) {
            if (plansza.getPlansza()[2][0].equals(plansza.getPlansza()[1][1]) && plansza.getPlansza()[1][1]
                    .equals(plansza.getPlansza()[0][2])) {
                return plansza.getPlansza()[0][0];
            }
        }
        return null;
    }
}
