import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.runners.Parameterized.*;


//@RunWith(value = Parameterized.class)
public class CalculatorTest {

    private Calculator calculator;
    private Integer numberA;
    private Integer numberB;
    private Integer expected;


//    public CalculatorTest( Integer numberA, Integer numberB, Integer expected) {
//
//        this.numberA = numberA;
//        this.numberB = numberB;
//        this.expected = expected;
//    }
//
//    @Parameters(name="{0}:testAdd({0}+{1}={2})")
//    public static Iterable<? extends Object> data(){
//        return Arrays.asList( new Object[][]{{1,1,2},{5,5,10},{3,8,11},});
//    }

    @Before
    public void setup() {
        calculator = new Calculator();
    }

    @Test
    public void shouldAddTwoNumbersAssertJ() {
        Assertions.assertThat(3 + 4).isEqualTo(7);
    }

    @Test
    public void shouldSubstractTwoNumbersAssertJ() {
        Assertions.assertThat(5 - 2).isEqualTo(3);
    }

    @Test
    public void shouldMultipleTwoNumbersAssertJ() {
        Assertions.assertThat(3 * 4).isEqualTo(12);
    }
//    @Test
//    public void testAdd(){
//        assertEquals(expected,calculator.add(numberA,numberB));
//    }

    @Test
    public void shouldAddTwoNumbers() {
        int result = calculator.add(3, 4);
        assertEquals(7, result);
    }

    @Test
    public void shouldSubstractTwoNumbers() {
        int result = calculator.subtract(5, 2);
        assertEquals(3, result);
    }

    @Test
    public void shouldMultipleTwoNumbers() {
        int result = calculator.multiple(3, 4);
        assertEquals(12, result);
    }

    @Test
    public void shouldDivTwoNumbers() {
        int result = calculator.divide(12, 4);
        assertEquals(3, result);
    }

    @Test
    public void shouldAddTwoNegativeNumbers() {
        int result = calculator.add(-5, -4);
        assertEquals(-9, result);
    }

    @Test
    public void shouldMultipleTwoNegativeNumbers() {
        int result = calculator.multiple(-3, 4);
        assertEquals(-12, result);
    }

    @Test
    public void shouldDivideByZeros() {
        try {
            calculator.divide(3, 0);

            fail();

        } catch (IllegalArgumentException e) {
            assertEquals("Divisor cannot be 0.",e.getLocalizedMessage());
            System.out.println(e.getLocalizedMessage());
        }
    }
}
