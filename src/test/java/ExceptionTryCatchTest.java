import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ExceptionTryCatchTest {
    private Calculator calculator;
    @Before
    public void setup(){
        calculator=new Calculator();
    }@Test
    public void shouldThrowIllegalEx(){
        try{
            calculator.divide(2,null);
            fail("Operation should have resulted in IllegalArgumentExecption.");

        }catch (IllegalArgumentException e){
            assertEquals(e.getMessage(),"cannot be null.");
        }
    }
}
