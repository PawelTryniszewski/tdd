import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class NotyficationServiceTest {


    EmailService emailService = Mockito.mock(EmailService.class);
    PigeonService pigeonService = Mockito.mock(PigeonService.class);

    NotyficationService notyficationService = new NotyficationService(emailService,pigeonService);
    @Test
    public  void shouldSendEmailWhenEmailServiceIsAvailable(){
        String message = "OK";
        when(emailService.isAvalible()).thenReturn(true);
        notyficationService.sendNotification(message);
        verify(emailService).sendEmail(message);

    }
}
