import org.assertj.core.api.Assertions;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TokenizerTest {
    private Tokenizer tokenizer = new Tokenizer();

    @Test
    public void shouldReturnEmptyListForNull() {
        Assertions.assertThat(tokenizer.tokenize1(null))
                .isEqualTo(new ArrayList<String>());
    }
    @Test
    public void shouldReturEmptyListnForEmptyString() {
        Assertions.assertThat(tokenizer.tokenize(""))
                .isEqualTo(new ArrayList<String>());
    }
    @Test
    public void shouldThrowIllegalException(){
        try{
            Assertions.assertThat(tokenizer.tokenize("ABC"))
                    .isEqualTo(new IllegalArgumentException());
            fail("Operation should have resulted in IllegalArgumentExecption.");

        }catch (IllegalArgumentException e){
            assertEquals("nic",e.getMessage());
        }
    }
    @Test
    public void shouldReturnOneElementListForString() {
        Assertions.assertThat(tokenizer.tokenize("1.0"))
                .isEqualTo(new ArrayList<String>(Arrays.asList("1.0")));
    }@Test
    public void shouldReturnThreeElementListForString() {
        Assertions.assertThat(tokenizer.tokenize("1+1"))
                .isEqualTo(new ArrayList<String>(Arrays.asList("1","+","1")));
    }@Test
    public void shouldReturnThreeElementListForOneDivTwoString() {
        Assertions.assertThat(tokenizer.tokenize("1/2"))
                .isEqualTo(new ArrayList<String>(Arrays.asList("1","/","2")));
    }


}
