package Gra;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PLanszaTest {
    Plansza plansza;
    @Before
    public void setup() {
        plansza = new Plansza();
    }
    @Test
    public void isEmpty() {
        boolean isEmpty = plansza.isEmpty();
        Assertions.assertThat(isEmpty).isEqualTo(true);
    }
    @Test
    public void shouldAddStringAtFirstIndex() {
        plansza.addElement("x",0,0);
        Assertions.assertThat(plansza.getPlansza()[0][0]).isEqualTo("x");
    }
    @Test
    public void shouldAddStringAtFirstIndexOnlyIfIndexEqualsNull() {
        plansza.addElement("x",0,0);
        plansza.addElement("o",0,0);
        Assertions.assertThat(plansza.getPlansza()[0][0]).isEqualTo("x");
    }
    @Test
    public void shouldNotAddStringAtIfIndeksIsNotNull() {
        plansza.addElement("x",0,1);
        plansza.addElement("o",0,1);
        Assertions.assertThat(plansza.getPlansza()[0][1]).isEqualTo("x");
    }
    @Test
    public void shouldNotAddStringAtIndeksIfStringIsIncorrect() {
        plansza.addElement("b",0,1);
        Assertions.assertThat(plansza.getPlansza()[0][1]).isEqualTo(null);
    }
    @Test
    public void shouldNotAddSecondTimeTheSameStringAtOneIndeks() {
        plansza.addElement("x",0,1);
        plansza.addElement("x",0,1);
        Assertions.assertThat(plansza.getPlansza()[0][1]).isEqualTo("x");
    }
    @Test
    public void shouldThrowOutOfBoundsExceptionsIfCoridnatesAreIncorrect(){
        try{
            plansza.addElement("x",0,6);
            fail("Operation should have resulted in IllegalArgumentExecption.");

        }catch (ArrayIndexOutOfBoundsException e){
            assertEquals(e.getMessage(),"Nie ma takiego miejsca.");
        }
    } @Test
    public void shouldThrowOutOfBoundsExceptionsIfCoridnatesAreIncorrectWhenWeGetWrongIndex(){
        try{
            plansza.getIndex(0,6);
            fail("Operation should have resulted in IllegalArgumentExecption.");

        }catch (ArrayIndexOutOfBoundsException e){
            assertEquals(e.getMessage(),"Nie ma takiego miejsca.");
        }
    }
    @Test
    public void shouldReturnStringOrNullFromCorrectCoordinates(){
        plansza.addElement("x",0,0);
        String result =plansza.getIndex(0,0);
        Assertions.assertThat(result).isEqualTo("x");
    }
}
