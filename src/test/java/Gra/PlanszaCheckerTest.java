package Gra;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class PlanszaCheckerTest {

    @Test
    public void shouldReturnFalseForEmptyBoard() {

        boolean isWinner = PlanszaChecker.isWinner(new Plansza());
        Assertions.assertThat(isWinner).isEqualTo(false);
    }
    @Test
    public void shoulrReturnSymbolIfAre3SameSymbolsInOneLine(){
        Plansza plansza = new Plansza();
        plansza.addElement("x",0,0);
//        plansza.addElement("x",0,1);
        plansza.addElement("x",0,2);
        String winner = PlanszaChecker.winner(plansza);
        Assertions.assertThat(winner).isEqualTo(null);

    }
    @Test
    public void shoulrReturnSymbolIfAre3SameSymbolsCross(){
        Plansza plansza = new Plansza();
        plansza.addElement("x",0,0);
        plansza.addElement("x",1,1);
        plansza.addElement("x",2,2);
        String winnerCross = PlanszaChecker.winnerCross(plansza);
        Assertions.assertThat(winnerCross).isEqualTo("x");

    }
    @Test
    public void shoulrReturnSymbolIfAre3SameSymbolsDown(){
        Plansza plansza = new Plansza();
        plansza.addElement("x",0,0);
        plansza.addElement("x",1,0);
        plansza.addElement("x",2,0);
        String winner = PlanszaChecker.winner(plansza);
        Assertions.assertThat(winner).isEqualTo("x");

    }
}
