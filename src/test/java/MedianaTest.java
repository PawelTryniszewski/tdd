import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MedianaTest {
    private Mediana mediana = new Mediana();

    @Test
    public void emptyArray(){
        int [] tab = new int[]{};
        int result = mediana.mediana(tab) ;
        assertEquals(-1,result);
    }@Test
    public void oneElementArray(){
        int [] tab = new int[]{1};
        int result = mediana.mediana(tab);
        assertEquals(tab[0],result);
    }@Test
    public void twoElementArray(){
        int [] tab = new int[]{1,2};
        int result = mediana.mediana(tab);
        assertEquals(1,result);
    }
    @Test
    public void oddArray(){
        int [] tab= new int[]{1,2,3,4};
        int result = mediana.mediana(tab) ;
        assertEquals(2,result);
    }@Test
    public void evenArray(){
        int [] tab = new int[]{5,2,1,4,3};
        int result = mediana.mediana(tab) ;
        assertEquals(3,result);
    }
}

