import org.junit.Before;
import org.junit.Test;

public class ExceptionExpectedTest {
    private Calculator calculator;
    @Before
    public void setup(){
        calculator=new Calculator();
    }
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalEx(){calculator.divide(2,0);}
}
