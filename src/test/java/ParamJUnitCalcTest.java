import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class ParamJUnitCalcTest {
    private Calculator calculator;

    @Before
    public void setup(){
        calculator=new Calculator();
    }
    @Test
    @Parameters({"-1,5,4","1,12,13","3,3,6"})
    public void testAdd(Integer a , Integer b , Integer expected){
        Integer result = calculator.add(a,b);
        assertEquals(expected,result);
    }
}
