import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NotyficationServiceTestRule {
    @Mock
    private static EmailService emailService;
    @Mock
    private static PigeonService pigeonService;

    @InjectMocks
    NotyficationService notyficationService = new NotyficationService(emailService, pigeonService);


    @Test
    public void shouldSendEmailWhenEmailServiceIsAvailable() {
        String message = "OK";
        when(emailService.isAvalible()).thenReturn(true);
        notyficationService.sendNotification(message);
        verify(emailService).sendEmail(message);
    }

    @Test
    public void shouldSendMessageWhenPigeonServiceIsAvailable(){
        String message = "OK";
        when(emailService.isAvalible()).thenReturn(false);
        when(pigeonService.isAvaliable()).thenReturn(true);
        notyficationService.sendNotification(message);
        verify(pigeonService).sendMessage(message);
    }
    @Test
    public void shouldSendNoMessageWhenServicesAreNotAvailable(){
        String message = "OK";
        //when(emailService.isAvalible()).thenReturn(false);
        //when(pigeonService.isAvaliable()).thenReturn(false);
        notyficationService.sendNotification(message);
        verify(pigeonService).sendMessage("Nie mozna wyslac");
    }
}
