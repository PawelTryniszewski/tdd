import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ExceptiorRuleTest {
    private Calculator calculator;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Before
    public void setup(){
        calculator=new Calculator();
    }
    @Test
    public void shouldThrwIllegalArgumentsException(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("cannot be null.");
        calculator.divide(2,null);

    }
}
